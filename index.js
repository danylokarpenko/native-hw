/**
 * @format
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';

import Root from './app/containers';
import store from './app/store';

class App extends React.Component {
    render() {
        return (
            <Root store={store} />
        )
    }
}

AppRegistry.registerComponent(appName, () => App);
