import * as React from 'react';
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native';

function Item({ item, navigation }) {
    return (
        <TouchableHighlight style={{ alignItems: 'center' }} onPress={() => navigation.navigate('Details', { item })}>
            <View style={styles.item}>
                <Text style={styles.title}>{item.displayName}</Text>
                <Text style={styles.number}>{item.phoneNumbers[0].number}</Text>
            </View>
        </TouchableHighlight>
    )
};

const styles = StyleSheet.create({
    item: {
        flex: 1,
        width: '90%',
        minWidth: 250,
        padding: 10,
        marginVertical: 10,
        backgroundColor: "#ccc",
        borderWidth: 1,
        borderColor: 'black'
    },
    title: {
        fontSize: 32,
    },
    number: {
        fontSize: 20,
        opacity: 0.5
    },
})

export default Item;