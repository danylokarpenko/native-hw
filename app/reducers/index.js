import { combineReducers } from 'redux';
import contacts from '../containers/ContactList/reducer';

const rootReducer = combineReducers({
    contacts
})

export default rootReducer;