import * as React from 'react';
import { connect } from 'react-redux';
import { PermissionsAndroid } from 'react-native';
import Contacts from "react-native-contacts";
import { bindActionCreators } from 'redux';
import { View, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import * as ContactsActions from './actions';
import Item from '../../components/ListItem/ListItem'
import SearchBar from '../Search/SearchBar';

class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchPlaceholder: 'Search'
        }
        this.search = this.search.bind(this);
    }

    async componentDidMount() {
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
            title: "Contacts",
            message: "This app would like to view your contacts."
        })
            .then(() => PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
                title: "Contacts",
                message: "This app would like to alter your contacts."
            }))
            .then(() => {
                this.loadContacts();
            });
    }

    loadContacts() {
        Contacts.getAll((err, contacts) => {
            if (err === "denied") {
                console.warn("Permission to access contacts was denied");
            } else {
                this.props.setContacts(contacts);
            }
        });
    }

    search(text) {
        const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
        const emailAddressRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
        if (text === "" || text === null) {
            this.loadContacts();
        } else if (phoneNumberRegex.test(text)) {
            Contacts.getContactsByPhoneNumber(text, (err, contacts) => {
                this.props.setContacts(contacts);
            });
        } else if (emailAddressRegex.test(text)) {
            Contacts.getContactsByEmailAddress(text, (err, contacts) => {
                this.props.setContacts(contacts);
            });
        } else {
            Contacts.getContactsMatchingString(text, (err, contacts) => {
                this.props.setContacts(contacts);
            });
        }
    }

    renderItem({ item }) {
        return (
            <Item item={item} navigation={this.props.navigation} />
        )
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <SearchBar
                    searchPlaceholder={this.state.searchPlaceholder}
                    onChangeText={this.search}
                />
                <FlatList
                    data={this.props.contacts}
                    renderItem={this.renderItem.bind(this)}
                    keyExtractor={item => item.recordID}
                />
                <TouchableOpacity
                    style={styles.addNew}
                    onPress={() => this.props.navigation.navigate('NewContact')}
                >
                    <Image
                        style={styles.logo}
                        source={{
                            uri:
                                'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg==',
                        }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    logo: {
        width: 40,
        height: 40,
    },
    addNew: {
        alignItems: "flex-start",
        width: 40,
        padding: 10
    }
})

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ContactsActions }, dispatch);
}

function mapStateToProps(state) {
    return {
        contacts: state.contacts
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);