import { ADD_CONTACT, UPDATE_CONTACT, DELETE_CONTACT, SET_CONTACTS } from './actionTypes';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_CONTACT: {
            const { id, data } = action.payload;
            const newContact = { recordID: id, ...data };
            return [...state, newContact];
        }

        case UPDATE_CONTACT: {
            const { id, data } = action.payload;
            const updatedContacts = state.map(contact => {
                if (contact.recordID === id) {
                    return {
                        ...contact,
                        ...data
                    };
                } else {
                    return contact;
                }
            })
            return updatedContacts;
        }

        case DELETE_CONTACT: {
            const { id } = action.payload;
            const filteredContacts = state.filter(contact => contact.recordID !== id);
            return filteredContacts;
        }

        case SET_CONTACTS: {
            const { contacts } = action.payload;
            return contacts;
        }

        default:
            return state;
    }
}