import { ADD_CONTACT, UPDATE_CONTACT, DELETE_CONTACT, SET_CONTACTS, SET_LIKE } from './actionTypes';
import service from './service';

export const addContact = (data) => ({
    type: ADD_CONTACT,
    payload: {
        id: service.getNewId(),
        data
    }
});

export const updateContact = (id, data) => ({
    type: UPDATE_CONTACT,
    payload: {
        id,
        data
    }
});

export const deleteContact = (id) => ({
    type: DELETE_CONTACT,
    payload: {
        id
    }
});

export const setContacts = (contacts) => ({
    type: SET_CONTACTS,
    payload: {
        contacts
    }
});
