import React, { Component } from 'react';
import { Provider } from 'react-redux';

import AppNavigator from '../../routes/StackNavigator/AppNavigator';

class Root extends Component {
    render() {
        return (
            <Provider store={this.props.store}>
                <AppNavigator />
            </Provider>
        )
    }
}

export default Root;