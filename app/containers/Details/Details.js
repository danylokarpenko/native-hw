import * as React from 'react';
import { Text, View, Button, StyleSheet, Image } from 'react-native';
import Contacts from "react-native-contacts";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import call from 'react-native-phone-call';
import * as ContactsActions from '../ContactList/actions';

function DetailsScreen({ deleteContact, route, navigation }) {
    const { item } = route.params;
    const renderItem = (item) => {
        return (
            <View style={styles.item}>
                <Image
                    style={styles.avatar}
                    source={require('../../assets/user.png')}
                />
                <Text style={styles.label}> Full Name </Text>
                <Text style={styles.title}>{item.displayName}</Text>
                <Text style={styles.label}> Phone Number </Text>
                <Text style={styles.title}>{item.phoneNumbers[0].number}</Text>
            </View>
        )
    }
    const onDelete = () => {
        Contacts.deleteContact(item, () => {
            deleteContact(item.recordID);
            navigation.navigate('Contacts');
        })
    }
    const onCall = () => {
        const number = item.phoneNumbers[0].number
        if (!number) return;
        const args = {
            number,
            prompt: false
        }
        call(args).catch(console.error)
    }
    return (
        <View style={styles.container}>
            {renderItem(item)}
            <View style={styles.buttons}>
                <Button title="Call" onPress={() => onCall()} />
                <Button title="Delete" onPress={() => onDelete()} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    item: {
        alignItems: 'flex-start'
    },
    label: {
        marginTop: 15,
        padding: 5,
    },
    container: {
        flex: 1,
        alignItems: 'flex-start',
        marginTop: 10,
        marginLeft: 50,
    },
    title: {
        fontSize: 32,
        marginBottom: 10
    },
    avatar: {
        alignItems: 'center',
        width: 90,
        height: 90,
    },
    buttons: {
        flexDirection: 'row'
    },
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ContactsActions }, dispatch);
}

function mapStateToProps(state) {
    return {
        contacts: state.contacts
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);