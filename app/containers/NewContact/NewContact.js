import * as React from 'react';
import { Text, View, Button, StyleSheet, TextInput } from 'react-native';
import Contacts from "react-native-contacts";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ContactsActions from '../ContactList/actions';

function NewContact({ addContact, navigation }) {
    const [givenName, setGivenName] = React.useState('');
    const [familyName, setFamilyName] = React.useState('');
    const [number, setNumber] = React.useState(null);

    const renderForm = () => {
        return (
            <View style={styles.form}>
                <Text style={styles.label}> Name </Text>
                <TextInput style={{ width: '90%', height: 40, borderColor: 'gray', borderWidth: 1 }}
                    placeholder='Name'
                    onChangeText={text => setGivenName(text)}
                    value={givenName}
                />
                <Text style={styles.label}> Surname </Text>
                <TextInput style={{ width: '90%', height: 40, borderColor: 'gray', borderWidth: 1 }}
                    placeholder='Surname'
                    onChangeText={text => setFamilyName(text)}
                    value={familyName}
                />
                <Text style={styles.label}> Phone Number </Text>
                <TextInput style={{ width: '90%', height: 40, borderColor: 'gray', borderWidth: 1 }}
                    placeholder='Phone Number'
                    onChangeText={text => setNumber(text)}
                    value={number}
                />
            </View>
        )
    }

    const onAddContact = () => {
        if (!givenName || !number) return;
        const newPerson = {
            phoneNumbers: [{
                label: "mobile",
                number
            }],
            familyName,
            givenName,
            displayName: `${givenName} ${familyName}`
        }
        Contacts.addContact(newPerson, (err) => {
            if (err) throw err;
            addContact(newPerson);
            navigation.goBack();
        })
    }
    
    return (
        <View style={styles.container}>
            {renderForm()}
            <View style={styles.buttons}>
                <Button title="Create" onPress={() => onAddContact()} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    form: {
        alignItems: 'center',
        width: '100%'
    },
    label: {
        marginTop: 15,
        padding: 5,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 10,
    },
    title: {
        fontSize: 32,
        marginBottom: 10
    },
    avatar: {
        alignItems: 'center',
        width: 90,
        height: 90,
    },
    buttons: {
        marginTop: 20,
        flexDirection: 'row'
    },
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...ContactsActions }, dispatch);
}

function mapStateToProps(state) {
    return {
        contacts: state.contacts
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewContact);