import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ContactList from '../../containers/ContactList/ContactList';
import Details from '../../containers/Details/Details';
import NewContact from '../../containers/NewContact/NewContact';

const Stack = createStackNavigator();

export default function AppNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Contacts" component={ContactList} />
                <Stack.Screen name="Details" component={Details} />
                <Stack.Screen name="NewContact" component={NewContact} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}